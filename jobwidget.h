#ifndef JOBWIDGET_H
#define JOBWIDGET_H

#include <QWidget>
#include <QMessageBox>
#include <QDateTime>

namespace Ui {
class jobWidget;
}

class jobWidget : public QWidget
{
    Q_OBJECT

public:
    explicit jobWidget(QWidget *parent = 0);
    ~jobWidget();
public slots:
    void addErrorLog(QString text);
    void addInfoLog(QString text);
    void setSeconds(int value);
    void setJobName(QString name);
    void setRate(QString rate);
signals:
    void startJob(QString jobName, QDateTime startTime);
    void pause();
    void saveLog(QString name, QString logText);
    void resume();
    void remove();
    void deleteJob();
    void askSeconds();
    void updateRate(double);
private slots:
    void on_jobSetCurrentTimeBtn_clicked();

    void on_jobStartBtn_clicked();

    void on_jobPauseBtn_clicked();

    void on_jobResumeBtn_clicked();

    void on_jobDeleteBtn_clicked();

    void on_rate_returnPressed();

    void recountCost();
private:
    Ui::jobWidget *ui;
    int seconds;
};

#endif // JOBWIDGET_H
