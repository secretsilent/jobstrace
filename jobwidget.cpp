#include "jobwidget.h"
#include "ui_jobwidget.h"

jobWidget::jobWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::jobWidget)
{
    ui->setupUi(this);
    ui->jobPauseBtn->hide();
    ui->jobResumeBtn->hide();
    ui->jobStartTime->setDateTime(QDateTime::currentDateTime());
    seconds = 0;
}

jobWidget::~jobWidget()
{
    emit deleteJob();
    delete ui;
}

void jobWidget::setSeconds(int value)
{
    this->seconds = value;
    recountCost();
}

void jobWidget::setJobName(QString name)
{
    ui->jobName->setText(name);
    ui->jobName->setEnabled(false);
}

void jobWidget::setRate(QString rate)
{
    ui->rate->setText(rate);
    emit updateRate(ui->rate->text().toDouble());
    recountCost();
}

void jobWidget::recountCost()
{
    double secondCost = ui->rate->text().toDouble()/3600;
    ui->cost->display(QString::number(secondCost*seconds, 'f', 2));
}

void jobWidget::on_jobSetCurrentTimeBtn_clicked()
{
    ui->jobStartTime->setDateTime(QDateTime::currentDateTime());
}

void jobWidget::on_jobStartBtn_clicked()
{
    if (ui->jobName->text().isEmpty())
        QMessageBox::warning(this, tr("Warning"), tr("Enter job name"));
    else {
        emit startJob(ui->jobName->text(), ui->jobStartTime->dateTime());
        emit updateRate(ui->rate->text().toDouble());
        ui->jobName->setEnabled(false);
        ui->jobStartTime->setEnabled(false);
        ui->jobSetCurrentTimeBtn->setEnabled(false);
        ui->jobStartBtn->setEnabled(false);
        ui->jobPauseBtn->show();
    }
}

void jobWidget::addErrorLog(QString text)
{
    ui->jobLog->insertHtml(QString("<font color=\"#ca0000\">%1</font><br>")
                           .arg(text));
}

void jobWidget::addInfoLog(QString text)
{
    ui->jobLog->insertPlainText(text+"\n");
}

void jobWidget::on_jobPauseBtn_clicked()
{
    emit pause();
    emit saveLog(ui->jobName->text(), ui->jobLog->document()->toPlainText());
    ui->jobResumeBtn->show();
    ui->jobPauseBtn->hide();
}

void jobWidget::on_jobResumeBtn_clicked()
{
    emit resume();
    ui->jobResumeBtn->hide();
    ui->jobPauseBtn->show();
}

void jobWidget::on_jobDeleteBtn_clicked()
{
    emit remove();
}

void jobWidget::on_rate_returnPressed()
{
    emit askSeconds();
    emit updateRate(ui->rate->text().toDouble());
}
