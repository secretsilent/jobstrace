#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QVector>
#include <QDateTime>
#include <QFile>
#include <QTextStream>
#include <jobwidget.h>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QDir>

namespace Ui {
class MainWindow;
}


class Job: public QObject
{
    Q_OBJECT
public:
    Job(int index) {
        totalSeconds = 0;
        name.clear();
        this->tabIndex = index;
        paused = true;
        startTime = QDateTime::currentDateTime();
        rate = 0;
    }
    ~Job();
signals:
    void info(QString);
    void error(QString);
    void setJobName(int, QString);
    void jobRemoved();
    void updateTotalSeconds(int);
public slots:
    void initialize(QString jobName, QDateTime startTime);
    void pause();
    void resume();
    void removeJob();
    void setRate(double value);
    void forceUpdateTotalSeconds();
    void setTotalSeconds(int secs);
    void setExistingJobName(QString name);
private slots:
    void saveToFile();
private:
    QDateTime startTime;
    QString name;
    QString fileName;
    qint64 totalSeconds;
    int tabIndex;
    bool paused;
    double rate;
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
signals:

private slots:
    void on_addJob_clicked();
    void saveLog(QString jobName, QString logText);
    void uploadExistingJobs();
    void createJob(QStringList values = QStringList());
    void tabDeleted();
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
