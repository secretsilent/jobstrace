#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->jobTabs->hide();
    QTimer::singleShot(0, this, SLOT(uploadExistingJobs()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

Job::~Job()
{

}

void Job::initialize(QString jobName, QDateTime startTime)
{
    this->name = jobName;
    QDir dir = QCoreApplication::applicationDirPath()+"/logs/";
    if (!dir.exists())
        dir.mkpath(QCoreApplication::applicationDirPath()+"/logs/");

    fileName = QCoreApplication::applicationDirPath()+"/logs/"+
            jobName+".csv";
    this->startTime = startTime;
    emit info(tr("Job starting time: %1")
              .arg(startTime.toString("dd.MM.yyyy hh:mm:ss")));
    emit setJobName(tabIndex, this->name);
    paused = false;
    forceUpdateTotalSeconds();
}

void Job::pause()
{
    paused = true;
    QDateTime dtime = QDateTime::currentDateTime();
    emit info(tr("-> On pause: %1")
              .arg(dtime.toString("dd.MM.yyyy hh:mm:ss")));
    totalSeconds += startTime.secsTo(dtime);
    double totalDuration = totalSeconds/3600;
    emit info(tr("Current total job duration: %1 hours (%3 minutes or "
                 "%2 seconds)")
              .arg(QString::number(totalDuration, 'f', 2))
              .arg(totalSeconds)
              .arg(QString::number(totalSeconds*1.0/60, 'f', 2)));
    emit updateTotalSeconds(totalSeconds);
    startTime = dtime;
    saveToFile();
}

void Job::forceUpdateTotalSeconds()
{
    QDateTime dtime = QDateTime::currentDateTime();
    int secs = paused?
                totalSeconds:
                totalSeconds+startTime.secsTo(dtime);
    emit updateTotalSeconds(secs);
}

void Job::setTotalSeconds(int secs)
{
    this->totalSeconds = secs;
    double totalDuration = totalSeconds/3600;
    emit info(tr("Already spent on the job %1 hours (%3 minutes or "
                 "%2 seconds)")
              .arg(QString::number(totalDuration, 'f', 2))
              .arg(totalSeconds)
              .arg(QString::number(totalSeconds*1.0/60, 'f', 2)));
}

void Job::setExistingJobName(QString name)
{
    emit setJobName(tabIndex, name);
    fileName = QCoreApplication::applicationDirPath()+"/logs/"+
            name+".csv";
}

void Job::resume()
{
    paused = false;
    QDateTime dtime = QDateTime::currentDateTime();
    emit info(tr("-> On resume: %1")
              .arg(dtime.toString("dd.MM.yyyy hh:mm:ss")));
    startTime = dtime;
}

void Job::removeJob()
{
    QFile file (fileName);
    if (!fileName.isEmpty()) {
        if (!file.remove()) {
            emit error(tr("Can't remove file %1")
                       .arg(fileName));
            return;
        }
    }
    emit jobRemoved();
}

void Job::setRate(double value)
{
    this->rate = value;
}

void Job::saveToFile()
{
    QFile file (fileName);
    if(file.open(QIODevice::WriteOnly))
    {
        QTextStream stream(&file);
        stream << QString("%1\n%2\n%3").arg(name).arg(totalSeconds)
                  .arg(rate);
        file.close();
    }
    else emit error(tr("Unable to open the file to save the results"));
}

void MainWindow::uploadExistingJobs()
{
    QDir dir(QCoreApplication::applicationDirPath()+"/logs");
    if (!dir.exists()) return;
    QStringList files = dir.entryList(QStringList("*.csv"),
                                      QDir::Files | QDir::Writable);
    if (files.isEmpty()) return;
    ui->jobTabs->show();
    ui->frame->hide();
    for(auto file_: files) {
        QFile file (dir.absoluteFilePath(file_));
        if (file.open(QIODevice::ReadOnly)) {
            // name - totalduration - rate
            QStringList values = QString(file.readAll()).split("\n");
            createJob(values);
            file.close();
        }
    }

}

void MainWindow::createJob(QStringList values)
{
    jobWidget *widget = new jobWidget;
    int index = ui->jobTabs->addTab(widget, tr("New job %1")
                                    .arg(ui->jobTabs->count()+1));
    Job *job = new Job(index);
    connect(widget, &jobWidget::startJob, job, &Job::initialize);
    connect(job, &Job::error, widget, &jobWidget::addErrorLog);
    connect(job, &Job::info, widget, &jobWidget::addInfoLog);
    connect(widget, &jobWidget::pause, job, &Job::pause);
    connect(widget, &jobWidget::resume, job, &Job::resume);
    connect(widget, &jobWidget::remove, job, &Job::removeJob);
    connect(job, &Job::setJobName, ui->jobTabs, &QTabWidget::setTabText);
    connect(job, &Job::jobRemoved, widget, &jobWidget::deleteLater);
    connect(widget, &jobWidget::deleteJob, job, &Job::deleteLater);
    connect(job, &Job::jobRemoved, this, &MainWindow::tabDeleted);
    connect(widget, &jobWidget::saveLog, this, &MainWindow::saveLog);
    connect(job, &Job::updateTotalSeconds, widget, &jobWidget::setSeconds);
    connect(widget, &jobWidget::askSeconds, job, &Job::forceUpdateTotalSeconds,
            Qt::DirectConnection);
    connect(widget, &jobWidget::updateRate, job, &Job::setRate);
    if (!values.isEmpty()) {
        widget->setJobName(values[0]);
        job->setExistingJobName(values[0]);
        job->setTotalSeconds(QString(values[1]).toInt());
        widget->setRate(values[2]);
    }

}

void MainWindow::tabDeleted()
{
    if (!ui->jobTabs) return;
    if (ui->jobTabs->count()-1 == 0) {
        ui->jobTabs->hide();
        ui->frame->show();
    }
}

void MainWindow::on_addJob_clicked()
{
    createJob();
    ui->jobTabs->show();
    ui->frame->hide();
}

void MainWindow::saveLog(QString jobName, QString logText)
{
    QFile file (QString(QCoreApplication::applicationDirPath()+
                        "/logs/%1_Log.txt").arg(jobName));
    if(file.open(QIODevice::WriteOnly))
    {
        QTextStream stream(&file);
        stream << logText;
        file.close();
    }
}
